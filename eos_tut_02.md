#  EOS Tutorial p.2: more features, better product?
...different product, I'd say....
## Background
You back! Either you're too curious or too stupid... Either way maybe you just need me, nothing more. Last time I left you with some script that creates a tangle out of code blocks in a .md file. Today's tutorial is about rewriting it all into c++.

Just kidding, I know you've already tested the c++ code and discovered that it was already all done for you (in c++11, is that a problem? If so, know that i might be drowning you in c++14 or 17 before you understand which train got you down).
However how you well understood the c++ code has the same capabilities, but also the limits as the bash script... And this for a c++ tut is bad news. In fact, in order to explain code whatsoever, I would need to go on sequentially with the approach we got. However, in a normal tutorial maybe one would want to start explaining away some of the code, then coming back to other; one might want to put all the code at the end of the article, etc, etc. However, with the current program approach we can't do that. We need to provide for this as soon as possible.

## What will we produce
Ok, we got some features we just discussed. Let's formalize them, and see what we got.

- Capability of handling macros, that is, to put a placeholder somewhere inside the code, and expand it later on with other code blocks. This entails:
    - ability of creating placeholders inside the code, 
    - ability of making a code block refer to the placeholder,
    - ability of append code to a codeblock, or substitute it altogether (versioning of the code).
- For tutorials that have to take into account different files at the same time (e.g., for a web app usually one needs a javascript file, a css file and a html file) we need the ability to refer a code block to the proper file (right now we can already handle multiple files, but only if they use different languages; if you need to explain two different html pages, you can't do it with our tangler). This might entail:
    - ability to redirect a code-block to a file.
    - ability to have multiple .md files discuss the same code (e.g., a tutorial in several parts. does it ring a bell?)

The last point brings us into another problem: the ability to handle projects, i.e., different .md files that can generate different source files... in few words:

- ability to **decouple**:
    - languages
    - source files created
    - .md files discussing them

## Refining the requirements
This last point got me thinking a lot (I know, it's not an activity I'm used to, that's why it makes my brain ache...).

I went over the list of feats I was writing before, and I got to prune it a little with the **decouple** scissors. This is what I got.

- **Decouple**:.md blocks code and source code --> decouple .MD's, code-blocks, sources, and languages, by using *macros expansion* and *reference to files*
-  **Decouple**: code blocks from their execution --> *hot code injection*

Notice how this is getting something much moire than your usual tangle... That's where the idea of eos started from...

**NOTICE**: I'm a son of a gun, I got much more to this then you can think right now... but I won't explain it until later tuts... He! He! He!

## Implementation
Ok, maybe all of the feats above, even without those I'm not telling you right now,  would be too much for one session alone... even worst, for now I'll have to write it all sequentially if I want to use one of the two tangle I got... so first things first, we need to decouple the block order in the .md file and the order inside the tangled source.

Thus we'll go about it sequentially for the last time.

...in c++, obviously...

#### .MD extensions
[Driusan](https://github.com/driusan/lmt) uses a special markdown title line before each block with the name wrapped inside quotes (```""```) to denote macro names, but I need a more expressive way...
I need the ability to denote filenames the block refers to and macro names in the same place, in order to decouple them better.

Most of his considerations are valid for me as well, but I'll proceed my merry way, anyways.

1. filenames and macros are to be expressed in the same way as namespaces in c++ filename(::macro(::sub-macro (... )))
2. let's put them inside .md syntax for blocks, i.e., lines that start with ```>```, but with a twist: we'll add a ! after it, like so: ```>!filename::macro``` or ```>!::macro``` if referring to the last/only file referred to.
