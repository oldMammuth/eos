#  EOS Tutorial p.1: Laying the foundation for a new programming language

## (To be read before starting out)
I was fiddling around [with Eve](http://witheve.com) lately (pun intended :-), and I got a lot of inspiration to jumpstart me again where I lost juice in my current projects.
To be honest I really have been ambivalent about literate programming: on the one hand it seems to me like the holy graal for tutorials and project cooperation; on the other hand it seems to mee that it could transform honest programming into a lot of ado for nothing.
Fact is, I really got to like some of the ideas I've got that I'd like to share with you. Personally, I think this stuff will be good for many, provided they give it a try. Ain't asking much, just read this first tut till it ends and figure out for yourself if this stuff is for you before going any further.

Ready? Go!

## Background
Couple of things to take into account for this first part:

- I'm going to be too practical at times: this seems to defile the whole point of literate programming, but it doesn't. LitProg is all about freedom, you up to choose how you wan't to use it.
- While I was working on these notes, I was thinking of intergrating a language with markdown (courstesy of eve). Imagine my surprise when one morning I saw [this](https://github.com/driusan/lmt) poject on github. It got me to focus more on the foundations of eos, and without it this first tut would not exist: *kudos to you, [driusan](https://github.com/driusan)!*
- this first tut is foundational, so I'm going to be less focused on eos...
- I'll use an agile approach to all of this, so I will create tools as we go, but don't expect all to be in their final form; expect them all to be useful and functioning, though, because that my philosophy: the whole point of being agile is to get something just functioning first, and to build on it as a foundation. And by being functioning I mean to be in a final form of sorts, not the final form I would want it to be, but a final form for anyone who wishes to bail from this train, so that I don't have to distribute refunds around.

## Tangles and weaves
In order to practice LitProg, one would usually need a tangle and a weaver to get code and documentation. driusan, who in his project built a tangle, argues that since he's using markdown, he doesn't need a weaver. I argue that if LitProg would be well done we wouldn't need either tangle or weave, because the point of LitProg should be to produce a file that is readable by both computers and men.

Eos is born from this consideration and some more, which I'll explain in later tutorials. Thus, without further ado, let's get hand on this mess, and see what is going to come out of it.

## What will we produce
As a first tut we will be producing a tangle. I said tangling is not one of my objectives, but we'll need this as a fundation for the next step we're going to take.

## Defining the features
Thinking about a tangle the first useful too one can come out of it is a copy and paste solution... Just kidding! copy and paste is useful, yet not really functioning... not on my book, at least.
What we need for a tangle is the following:

- basic humans' languages tools --> weaver
- basic computer languages tools --> tangle

The first can be covered by markdown. Which has, as a side bonus, the possibility of embedding code and specifying the syntax for it.For the second I was thinking of a scripting language... someone want to give it a try with bash scripts?

## First implementation
The first imp of a tangle has to have at least these functions:

- search sequentially for code blocks
- extrapolate them and put them into a code file.

This first imp supports only sequential code description, and no macros whatsoever.

> tangle.sh
``` bash
#! /bin/sh
```
The first thing we do is to declare the shebang for a shellscript.

Next, we'll declare the variables the script will accept. Note that in ```$0``` you get the script's own name.
``` bash
LANG=$1
INPUT=$2
OUTPUT=$3
```
The options the script will accept, then are:

- *LANG*: the language of the block: in this way we can mix different languages in the same .md file and get obnly what we need. This is especially useful to distinguish in a tutorial between commands to pass to a shell (LANG=bash), and proper code.
- *INPUT*: the .md file to parse.
- *OUTPUT*: the code file to write the tangle into.

Next we'll use ```awk``` to extract the lines between the

    \`\`\` $LANG
    \`\`\` 

``` bash
awk '/^``` '$LANG'$/,/^```$/ { if ($0 != "``` '$LANG'" && $0 != "```") print }' $INPUT >> $OUTPUT
```
Note how this code detects the lines of text between the two block delimiters (\`\`\` $LANG, and \`\`\`), with the expantion of the variable ```$LANG``` right inside the delimiters. the code to be executed for each line gets passed to *awk* in the {...} block.
Since the two delimiters are identified as well, we need to pass a condition to exclude the two delimiters from the printed output. 

Finally, the printing is redirected to the file passed as the third command-line argument.

**LIMITATION**: the code doesn't stand blank space freely used, i.e., there must be a white space between \`\`\` and LANG, and no whitespace after the closing \`\`\` !!!

## Self-building
In order to try the tangle you need to copy the script into a *test.sh*, give it *executable* permissions, then run it like this:

```
$ ./test.sh bash eos_tut_01.md tangle.sh
```

If all goes well, this command should output that there are no differences between the two files:

```
$ diff test.sh tangle.sh
```
Upon success, *diff* should not return a thing at all.

From now on, you could use your *tangle.sh*  to get the tangling job done. (Set *tangle.sh*'s permissions to executable first... and delete *test.sh* too! A little secret: i'm so maniac I tried again tangle.sh over the README.md, and made the diff between the two... Don't look at me like that, I know you've thought about it too!)

## Conclusions
Thus, you have created a tangle that tangles itself from this *README.md* file.

Granted, to bootstrap this process, you needed to copy and paste the code yourself. This may not seem much to you, because this tool really needs a lot more to become really a useful thing, but hold fast to it for the time being. All in all, it already accomplishes a lot with such little code written.

You can jump now to the [second installment](eos_tut_02.md) of this tutorial.

#### Post Scriptum
This feature about multiple langs support is really nice... I might be fiddling with it some more like:

``` cpp
#include <string>
#include <iostream>
#include <regex>
#include <fstream>

int main(int argc, char** argv){
    if (argc < 3){
        std::cout << "Usage: " << argv[0] << " LANG inputfile outputfile" << std::endl;
        return 1;
    }
    std::string lang = std::string(argv[1]);
	std::string _inf = std::string(argv[2]);
	std::string _outf = std::string(argv[3]);
	
	std::ifstream inputfile;
	std::ofstream outputfile;
	std::regex _begin("``` " + lang);
	std::regex _end("```");
	std::string line;
	bool inside_the_block=false;
	
	inputfile.open(_inf);
    outputfile.open(_outf);
	
	if(inputfile.is_open() && outputfile.is_open()){
		while(inputfile.good()){
			getline(inputfile,line);
            if (inside_the_block == false){
				if(regex_match(line, _begin)){
					inside_the_block = true;
				}
			}
			else {
				if(regex_match(line, _end)){
					inside_the_block = false;
				}
				else outputfile << line << std::endl;
			}
				
		}
	}
    return 0;    
}
```
