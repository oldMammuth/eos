# EOS Programming Language
Lucky you! You have stumbled upon a series of tutorials about a new programming language that I'm creating. Or is it?

In this series I'll write and explain over a lot of code, but **I will not provide any ready-made program for you**. 
You heard me well, It's a new practice I'm enforcing, by which at least you will have to read few tuts before getting 
the program you want in order to get the rest of the project.

It's for your own good, trust me... I think...

Well, without further ado, let's jump to the [first tutorial](eos_tut_01.md)!

#### Post Scriptum

**TOC**:

- tut 01: Starting out our journey. We'll get a shell written poor man's tangle. 
*TODO*: rough edges in writing form, I don't like the current style; It might require a complete makeover.
- tut 01: Doubling in c++ and adding features. Our tangle gets better by the hour.
